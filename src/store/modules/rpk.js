import apiFetch from '../fetch'

export default {
    namespaced: true,

    state: {
        isLoading: false,
        notice: null,
        list: [],
        item: null,
        rows: 0,

        employers: [],
        departments: [],
        'industry-projects': [],
        directions: [],
        institutions: [],

        person: null,
        questionary: null,
        request: null,
        documents: [],

        egeResults: null,
    },

    getters: {
        isLoading (state) {
            return state.isLoading
        },

        notice (state) {
            return state.notice
        },

        list (state) {
            return state.list
        },

        item (state) {
            return state.item
        },

        rows (state) {
            return state.rows
        },

        employers (state) {
            return state.employers
        },

        departments (state) {
            return state.departments
        },

        'industry-projects' (state) {
            return state['industry-projects']
        },

        directions (state) {
            return state.directions
        },

        institutions (state) {
            return state.institutions
        },

        person (state) {
            return state.person
        },

        questionary (state) {
            return state.questionary
        },

        request (state) {
            return state.request
        },

        documents (state) {
            return state.documents
        },

        egeResults (state) {
            return state.egeResults
        },
    },

    mutations: {
        setIsLoading (state, value) {
            state.isLoading = value
        },

        setNotice (state, value) {
            state.notice = value
        },

        setList (state, value) {
            state.list = value
        },

        setItem (state, value) {
            state.item = value
        },

        setRows (state, value) {
            state.rows = value
        },

        setModelList (state, data) {
            state[data.model] = data.value
        },

        setPerson (state, value) {
            state.person = value
        },

        setQuestionary (state, value) {
            state.questionary = value
        },

        setRequest (state, value) {
            state.request = value
        },

        setDocuments (state, value) {
            state.documents = value
        },

        setEgeResults (state, value) {
            state.egeResults = value
        },
    },

    actions: {
        async getList (ctx, data) {
            ctx.commit('setIsLoading', true)

            const res = await apiFetch({
                url: `rpk/${data.model}`,
                method: 'get',
                params: data.params ? data.params : null,
            })

            if (res.ok) {
                let json = await res.json()

                if (data.relation) {
                    ctx.commit('setModelList', {
                        model: data.model,
                        value: json.results
                    })
                } else {
                    ctx.commit('setList', json.results)
                }

                ctx.commit('setRows', json.count)
                ctx.commit('setIsLoading', false)
            }
        },

        async getItem (ctx, data) {
            ctx.commit('setIsLoading', true)

            let stringId = ''

            if (data.id) {
                stringId += `/${data.id}`
            }

            const res = await apiFetch({
                url: `rpk/${data.model}${stringId}`,
                method: 'get',
            })

            if (res.ok) {
                let json = await res.json()

                ctx.commit('setItem', json)
                ctx.commit('setIsLoading', false)
            }
        },

        async getPersonData (ctx, data) {
            ctx.commit('setIsLoading', true)

            const res = await apiFetch({
                url: `rpk/persons/${data.id}`,
                method: 'get',
            })

            if (res.ok) {
                const json = await res.json()
                ctx.commit('setPerson', json)

                const questionaryRes = await apiFetch({
                    url: `rpk/questionaries`,
                    method: 'get',
                    params: {
                        person: json.id,
                        limit: 1000,
                    }
                })

                if (questionaryRes.ok) {
                    const questionaryJson = await questionaryRes.json()

                    ctx.commit('setQuestionary', questionaryJson.results[0])
                }

                const requestRes = await apiFetch({
                    url: `rpk/requests`,
                    method: 'get',
                    params: {
                        person: json.id,
                        limit: 1000,
                    }
                })

                if (requestRes.ok) {
                    const requestJson = await requestRes.json()

                    ctx.commit('setRequest', requestJson.results[0])
                }

                const docsRes = await apiFetch({
                    url: `rpk/documents`,
                    method: 'get',
                    params: {
                        person: json.id,
                        limit: 1000,
                    }
                })

                if (docsRes.ok) {
                    const docsJson = await docsRes.json()

                    ctx.commit('setDocuments', docsJson.results)
                }

                const egeResultsRes = await apiFetch({
                    url: `rpk/ege-results`,
                    method: 'get',
                    params: {
                        person: json.id,
                        limit: 1000,
                    }
                })

                if (egeResultsRes.ok) {
                    const egeResultsJson = await egeResultsRes.json()

                    ctx.commit('setEgeResults', egeResultsJson.results[0])
                }

                ctx.commit('setIsLoading', false)
            }
        },

        async save (ctx, data) {
            ctx.commit('setIsLoading', true)

            let fData = {}

            for (let i in data.fields) {
                let field = data.fields[i]

                if (field.value) {
                    if (field.type === 'image') {
                        
                        if (field.value.file) {
                            if (typeof(field.value.file) === 'object') fData[field.slug] = field.value.file
                        } else if (field.value.file === null && field.value.url === null) {
                            if (field.value.file === null) fData[field.slug] = ''
                        }
                    } else if (field.type === 'checkbox') {
                        fData[field.slug] = field.value ? 1 : 0
                    } else {
                        fData[field.slug] = field.value
                    }
                }
            }

            let stringId = ''

            if (data.id) {
                stringId += `/${data.id}`
            }

            const res = await apiFetch({
                url: `rpk/${data.model}${stringId}`,
                method: data.id ? 'put' : 'post',
                data: fData,
            })

            ctx.commit('setIsLoading', false)

            if (res.ok) {
                data.router.push({ path: `/rpk/${data.model}` })

                ctx.commit('setNotice', {
                    text: 'Запись успешно сохранена',
                    type: 'success',
                })
            } else {
                ctx.commit('setNotice', null)
            }
        },

        async deleteItem (ctx, data) {
            ctx.commit('setIsLoading', true)

            const res = await apiFetch({
                url: `rpk/${data.model}/${data.id}`,
                method: 'delete'
            })

            if (res.ok) {
                ctx.commit('setItem', null)
                ctx.commit('setIsLoading', false)

                ctx.commit('setNotice', {
                    text: 'Запись успешно удалена',
                    type: 'success'
                })

                data.router.push({ path: '/rpk/' + data.model })
            }
        },

        async exportPersons (ctx, data) {
            ctx.commit('setIsLoading', true)

            const res = await apiFetch({
                method: 'post',
                url: `rpk/persons/export`
            })

            if (res.ok) {
                const json = await res.json()

                ctx.commit('setIsLoading', false)

                if (json.success) {
                    let a = document.createElement('a')
                    a.href = json.file_url
                    a.target = '_blank'

                    document.body.appendChild(a)
                    a.click()
                    document.body.removeChild(a)
                }
            }
        }
    }
}