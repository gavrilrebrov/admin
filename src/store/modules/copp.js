import VueCookies from 'vue-cookies'
import queryString from 'query-string'

let apiUrl = 'https://platform.grebrov.space/api/'

if (process.env.NODE_ENV === 'development') {
    apiUrl = 'http://localhost:8000/api/'
}

export default {
    namespaced: true,

    state: {
        sc: {
            institutions: {
                list: [],
                item: null,
            },
            programs: {
                list: [],
                item: null,
            },
            requests: [],
        },
        busyness: {
            requests: [],
            request: null,
            questionnaires: [],
            documents: [],
            testResults: [],
            tutors: [],
            tutor: null,
        },
        notice: null,
        isLoading: false,
        rows: 0,

        directions: [],
    },

    getters: {
        scInstitutions (state) {
            return state.sc.institutions.list
        },

        scInstitution (state) {
            return state.sc.institutions.item
        },

        scPrograms (state) {
            return state.sc.programs.list
        },

        scProgram (state) {
            return state.sc.programs.item
        },

        notice (state) {
            return state.notice
        },

        isLoading (state) {
            return state.isLoading
        },

        rows (state) {
            return state.rows
        },

        scRequests (state) {
            return state.sc.requests
        },

        busynessRequests (state) {
            return state.busyness.requests
        },

        busynessRequest (state) {
            return state.busyness.request
        },

        busynessDocuments (state) {
            return state.busyness.documents
        },

        busynessQuestionnaires (state) {
            return state.busyness.questionnaires
        },

        busynessTestResults (state) {
            return state.busyness.testResults
        },

        busynessTutors (state) {
            return state.busyness.tutors
        },

        busynessTutor (state) {
            return state.busyness.tutor
        },

        directions (state) {
            return state.directions
        }
    },

    actions: {
        async saveScProgram (ctx, data) {
            ctx.commit('setIsLoading', true)

            let formData = new FormData()
            formData.append('name', data.form.name)

            if (data.form.institution) formData.append('institution', data.form.institution)
            formData.append('type', data.form.type)
            formData.append('format', data.form.format)
            formData.append('form', data.form.form)
            formData.append('hours', data.form.hours)
            formData.append('period', data.form.period)
            formData.append('cost', data.form.cost)
            if (data.form.isCopp) formData.append('is_copp', 1)

            let url = apiUrl + 'copp/sc/programs/'

            if (data.form.id) {
                url += data.form.id + '/'
            }

            const res = await fetch(url, {
                method: data.form.id ? 'put' : 'post',
                body: formData,
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            ctx.commit('setIsLoading', false)

            if (res.ok) {
                data.vm.$router.push({ path: '/copp/sc/programs' })

                ctx.commit('setNotice', {
                    text: 'Запись успешно сохранена',
                    type: 'success',
                })
            } else {
                ctx.commit('setNotice', null)
            }
        },

        async saveScInstitution (ctx, data) {
            ctx.commit('setIsLoading', true)

            let formData = new FormData()
            formData.append('name', data.form.name)
            formData.append('short_name', data.form.short_name)
            
            formData.append('group', data.form.group)

            if (data.form.description) formData.append('description', data.form.description)
            if (data.form.image && typeof(data.form.image) === 'object') formData.append('image', data.form.image)
            if (data.form.image === null) formData.append('image', '')

            let url = apiUrl + 'copp/sc/institutions/'

            if (data.form.id) {
                url += data.form.id + '/'
            }

            const res = await fetch(url, {
                method: data.form.id ? 'put' : 'post',
                body: formData,
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            ctx.commit('setIsLoading', false)

            if (res.ok) {
                data.vm.$router.push({ path: '/copp/sc/institutions' })

                ctx.commit('setNotice', {
                    text: 'Запись успешно сохранена',
                    type: 'success',
                })
            } else {
                ctx.commit('setNotice', null)
            }
        },

        async saveBusynessTutor (ctx, data) {
            ctx.commit('setIsLoading', true)

            let formData = new FormData()
            formData.append('name', data.vm.form.name)

            let url = apiUrl + 'copp/busyness/tutors/'

            if (data.vm.$route.params.id) {
                url += data.vm.$route.params.id + '/'
            }

            const res = await fetch(url, {
                method: data.vm.$route.params.id ? 'put' : 'post',
                body: formData,
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            ctx.commit('setIsLoading', false)

            if (res.ok) {
                data.vm.$router.push({ path: '/copp/busyness/tutors' })

                ctx.commit('setNotice', {
                    text: 'Запись успешно сохранена',
                    type: 'success',
                })
            } else {
                ctx.commit('setNotice', null)
            }
        },

        async save (ctx, data) {
            ctx.commit('setIsLoading', true)

            let formData = new FormData()

            for (let i in data.fields) {
                let field = data.fields[i]

                if (field.value) {
                    if (field.type === 'image') {
                        
                        if (field.value.file) {
                            if (typeof(field.value.file) === 'object') formData.append(field.slug, field.value.file)
                        } else if (field.value.file === null && field.value.url === null) {
                            if (field.value.file === null) formData.append(field.slug, '')
                        }
                    } else if (field.type === 'checkbox') {
                        formData.append(field.slug, field.value ? 1 : 0)
                    } else {
                        formData.append(field.slug, field.value)
                    }
                }
            }

            let url = `${apiUrl}copp/${data.model}/`

            if (data.vm.$route.params.id) {
                url += data.vm.$route.params.id + '/'
            }

            const res = await fetch(url, {
                method: data.vm.$route.params.id ? 'put' : 'post',
                body: formData,
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            ctx.commit('setIsLoading', false)

            if (res.ok) {
                data.vm.$router.push({ path: `/copp/${data.model}` })

                ctx.commit('setNotice', {
                    text: 'Запись успешно сохранена',
                    type: 'success',
                })
            } else {
                ctx.commit('setNotice', null)
            }
        },

        async getScInstitutions (ctx, params = null) {
            ctx.commit('setIsLoading', true)
            ctx.commit('setRows', 0)

            let query = ''

            if (params) {
                query = '?' + queryString.stringify(params)
            }

            const res = await fetch(apiUrl + 'copp/sc/institutions/' + query, {
                method: 'get',
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            if (res.ok) {
                const json = await res.json()

                ctx.commit('setScInstitutions', json.results)
                ctx.commit('setRows', json.count)
            }

            ctx.commit('setIsLoading', false)
        },

        async getScPrograms (ctx, params = null) {
            ctx.commit('setIsLoading', true)
            ctx.commit('setRows', 0)

            let query = ''

            if (params) {
                query = '?' + queryString.stringify(params)
            }

            const res = await fetch(apiUrl + 'copp/sc/programs/' + query, {
                method: 'get',
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            if (res.ok) {
                const json = await res.json()

                ctx.commit('setScPrograms', json.results)
                ctx.commit('setRows', json.count)
            }

            ctx.commit('setIsLoading', false)
        },

        async getScRequests (ctx, params = null) {
            ctx.commit('setIsLoading', true)
            ctx.commit('setRows', 0)

            let query = ''

            if (params) {
                query = '?' + queryString.stringify(params)
            }

            const res = await fetch(apiUrl + 'copp/sc/requests/' + query, {
                method: 'get',
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            if (res.ok) {
                const json = await res.json()

                ctx.commit('setScRequests', json.results)
                ctx.commit('setRows', json.count)
            }

            ctx.commit('setIsLoading', false)
        },

        async getBusynessRequests (ctx, params = null) {
            ctx.commit('setIsLoading', true)
            ctx.commit('setRows', 0)

            let query = ''

            if (params) {
                query = '?' + queryString.stringify(params)
            }

            const res = await fetch(apiUrl + 'copp/busyness/requests/' + query, {
                method: 'get',
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            if (res.ok) {
                const json = await res.json()

                ctx.commit('setBusynessRequests', json.results)
                ctx.commit('setRows', json.count)
            }

            ctx.commit('setIsLoading', false)
        },

        async getBusynessTutors (ctx, params = null) {
            ctx.commit('setIsLoading', true)
            ctx.commit('setRows', 0)

            let query = ''
            if (params) {
                query = '?' + queryString.stringify(params)
            }

            const res = await fetch(apiUrl + 'copp/busyness/tutors/' + query, {
                method: 'get',
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            if (res.ok) {
                const json = await res.json()

                ctx.commit('setBusynessTutors', json.results)
                ctx.commit('setRows', json.count)
            }

            ctx.commit('setIsLoading', false)
        },

        async getDirections ({ commit }, params) {
            commit('setIsLoading', true)
            commit('setRows', 0)

            let query = ''
            if (params) {
                query = '?' + queryString.stringify(params)
            }

            const res = await fetch(apiUrl + 'copp/events/directions/' + query, {
                method: 'get',
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            if (res.ok) {
                const json = await res.json()

                commit('directions', json.results)
                commit('setRows', json.count)
            }

            commit('setIsLoading', false)
        },

        async getBusynessTutor (ctx, id) {
            ctx.commit('setIsLoading', true)

            const res = await fetch(apiUrl + 'copp/busyness/tutors/' + id + '/', {
                method: 'get',
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            if (res.ok) {
                const json = await res.json()

                ctx.commit('setBusynessTutor', json)
            }

            ctx.commit('setIsLoading', false)
        },

        

        async getScInstitution (ctx, id) {
            ctx.commit('setIsLoading', true)

            const res = await fetch(apiUrl + 'copp/sc/institutions/' + id + '/', {
                method: 'get',
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            if (res.ok) {
                const json = await res.json()

                ctx.commit('setScInstitution', json)
            }

            ctx.commit('setIsLoading', false)
        },

        async getBusynessRequest (ctx, id) {
            ctx.commit('setIsLoading', true)

            const res = await fetch(apiUrl + 'copp/busyness/requests/' + id + '/', {
                method: 'get',
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            if (res.ok) {
                const json = await res.json()

                ctx.commit('setBusynessRequest', json)

                const docsRes = await fetch(`${apiUrl}copp/busyness/documents?request=${json.id}&limit=1000`, {
                    method: 'get',
                    headers: {
                        Authorization: 'Token ' + VueCookies.get('token')
                    }
                })

                if (docsRes.ok) {
                    let doc = await docsRes.json()
                    ctx.commit('setBusynessDocuments', doc.results)
                }

                const qRes = await fetch(`${apiUrl}copp/busyness/questionnaires?request=${json.id}&limit=1000`, {
                    method: 'get',
                    headers: {
                        Authorization: 'Token ' + VueCookies.get('token')
                    }
                })

                if (qRes.ok) {
                    let qJson = await qRes.json()
                    ctx.commit('setBusynessQuestionnaires', qJson.results)
                }

                const trRes = await fetch(`${apiUrl}copp/busyness/test-results?request=${json.id}&limit=1000`, {
                    method: 'get',
                    headers: {
                        Authorization: 'Token ' + VueCookies.get('token')
                    }
                })

                if (trRes.ok) {
                    let trJson = await trRes.json()
                    ctx.commit('setBusynessTestResults', trJson.results)
                }
            }

            ctx.commit('setIsLoading', false)
        },

        async getScProgram (ctx, id) {
            ctx.commit('setIsLoading', true)

            const res = await fetch(apiUrl + 'copp/sc/programs/' + id + '/', {
                method: 'get',
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            if (res.ok) {
                const json = await res.json()

                ctx.commit('setScProgram', json)
            }

            ctx.commit('setIsLoading', false)
        },

        async deleteScInstitution (ctx, data) {
            ctx.commit('setIsLoading', true)

            const res = await fetch(apiUrl + 'copp/sc/institutions/' + data.id + '/', {
                method: 'delete',
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            if (res.ok) {
                ctx.commit('setScInstitution', null)

                ctx.commit('setIsLoading', false)

                data.vm.$router.push({ path: '/copp/sc/institutions' })

                ctx.commit('setNotice', {
                    text: 'Запись успешно удалена',
                    type: 'success',
                })
            }
        },

        async deleteScProgram (ctx, data) {
            ctx.commit('setIsLoading', true)

            const res = await fetch(apiUrl + 'copp/sc/programs/' + data.id + '/', {
                method: 'delete',
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            if (res.ok) {
                ctx.commit('setScProgram', null)

                ctx.commit('setIsLoading', false)

                data.vm.$router.push({ path: '/copp/sc/programs' })

                ctx.commit('setNotice', {
                    text: 'Запись успешно удалена',
                    type: 'success',
                })
            }
        },

        async deleteBusynessTutor(ctx, data) {
            ctx.commit('setIsLoading', true)

            const res = await fetch(`${apiUrl}copp/busyness/tutors/${data.vm.$route.params.id}/`, {
                method: 'delete',
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            if (res.ok) {
                ctx.commit('setBusynessTutor', null)
                ctx.commit('setIsLoading', false)

                data.vm.$router.push({ path: '/copp/busyness/tutors' })

                ctx.commit('setNotice', {
                    text: 'Запись успешно удалена',
                    type: 'success',
                })
            }
        },

        async import (ctx, data) {
            const formData = new FormData()

            ctx.commit('setIsLoading', true)

            formData.append('institution', data.form.institution)
            
            if (data.form.file) formData.append('file', data.form.file)

            const res = await fetch(apiUrl + 'copp/sc/programs/import', {
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                },
                method: 'post',
                body: formData,
            })

            if (res.ok) {
                const json = await res.json()

                ctx.commit('setIsLoading', false)

                data.vm.$router.push({ path: '/copp/sc/programs' })

                ctx.commit('setNotice', {
                    text: 'Программ добавлено ' + json.count,
                    type: 'success'
                })
            }
        },

        async scDownloadRequests (ctx, data) {
            ctx.commit('setIsLoading', true)

            const formData = new FormData()

            if (data.vm.filter) {
                formData.append('start_date', data.vm.filter.start_date)
                formData.append('end_date', data.vm.filter.end_date)
                formData.append('search', data.vm.filter.search)
                formData.append('institution', data.vm.filter.institution)
            }

            const res = await fetch(apiUrl + 'copp/sc/requests/export', {
                method: 'post',
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                },
                body: formData,
            })

            if (res.ok) {
                const json = await res.json()

                ctx.commit('setIsLoading', false)

                if (json.success && json.file_url) {
                    let a = document.createElement('a')
                    a.href = json.file_url
                    a.target = '_blank'

                    document.body.appendChild(a)
                    a.click()
                    document.body.removeChild(a)
                }
            }
        },

        async downloadBusynessDocuments (ctx, id) {
            ctx.commit('setIsLoading', true)

            const res = await fetch(`${apiUrl}copp/client/busyness/documents/zip/?id=${id}`, {
                method: 'get',
                headers: {
                    Authorization: 'Token ' + VueCookies.get('token')
                }
            })

            if (res.ok) {
                const json = await res.json()

                let a = document.createElement('a')
                a.href = json.url
                a.target = '_blank'

                document.body.appendChild(a)
                a.click()
                document.body.removeChild(a)
            }

            ctx.commit('setIsLoading', false)
        }
    },

    mutations: {
        setScInstitutions (state, value) {
            state.sc.institutions.list = value
        },

        setScInstitution (state, value) {
            state.sc.institutions.item = value
        },

        setNotice (state, value) {
            state.notice = value
        },

        setIsLoading (state, value) {
            state.isLoading = value
        },

        setScPrograms (state, value) {
            state.sc.programs.list = value
        },

        setScProgram (state, value) {
            state.sc.programs.item = value
        },

        setRows (state, value) {
            state.rows = value
        },

        setScRequests (state, value) {
            state.sc.requests = value
        },

        setBusynessRequests (state, value) {
            state.busyness.requests = value
        },

        setBusynessRequest (state, value) {
            state.busyness.request = value
        },

        setBusynessDocuments (state, value) {
            state.busyness.documents = value
        },

        setBusynessQuestionnaires (state, value) {
            state.busyness.questionnaires = value
        },

        setBusynessTestResults (state, value) {
            state.busyness.testResults = value
        },

        setBusynessTutor (state, value) {
            state.busyness.tutor = value
        },

        setBusynessTutors (state, value) {
            state.busyness.tutors = value
        },

        directions (state, value) {
            state.directions = value
        }
    }
}