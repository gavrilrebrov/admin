import apiFetch from '../fetch'

export default {
    namespaced: true,

    state: {
        list: [],
        item: null,
        members: [],
        rows: 0,
    },

    getters: {
        list (state) {
            return state.list
        },

        item (state) {
            return state.item
        },

        members (state) {
            return state.members
        },

        rows (state) {
            return state.rows
        },
    },

    mutations: {
        setList (state, value) {
            state.list = value
        },

        setItem (state, value) {
            state.item = value
        },

        setMembers (state, value) {
            state.members = value
        },

        setRows (state, value) {
            state.rows = value
        },
    },

    actions: {
        async getList (ctx, data) {
            
        }
    }
}