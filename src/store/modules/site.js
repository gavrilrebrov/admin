import apiFetch from '../fetch'

export default {
    namespaced: true,

    state: {
        isLoading: false,
        notice: null,
        list: [],
        item: null,
        rows: 0,
    },

    getters: {
        isLoading (state) {
            return state.isLoading
        },

        notice (state) {
            return state.notice
        },

        list (state) {
            return state.list
        },

        item (state) {
            return state.item
        },

        rows (state) {
            return state.rows
        }
    },

    mutations: {
        setIsLoading (state, value) {
            state.isLoading = value
        },

        setNotice (state, value) {
            state.notice = value
        },

        setList (state, value) {
            state.list = value
        },

        setItem (state, value) {
            state.item = value
        },

        setRows (state, value) {
            state.rows = value
        }
    },

    actions: {
        async save (ctx, data) {
            ctx.commit('setIsLoading', true)

            let fData = {}

            for (let i in data.fields) {
                let field = data.fields[i]

                if (field.value) {
                    if (field.type === 'image') {
                        
                        if (field.value.file) {
                            if (typeof(field.value.file) === 'object') fData[field.slug] = field.value.file
                        } else if (field.value.file === null && field.value.url === null) {
                            if (field.value.file === null) fData[field.slug] = ''
                        }
                    } else if (field.type === 'checkbox') {
                        fData[field.slug] = field.value ? 1 : 0
                    } else {
                        fData[field.slug] = field.value
                    }
                }
            }

            let stringId = ''

            if (data.id) {
                stringId += `/${data.id}`
            }

            const res = await apiFetch({
                url: `app/${ctx.rootGetters.project.slug}/${data.model}${stringId}`,
                method: data.id ? 'put' : 'post',
                data: fData,
            })

            ctx.commit('setIsLoading', false)

            if (res.ok) {
                data.router.push({ path: `/site/${data.model}` })

                ctx.commit('setNotice', {
                    text: 'Запись успешно сохранена',
                    type: 'success',
                })
            } else {
                ctx.commit('setNotice', null)
            }
        },

        async getItem (ctx, data) {
            ctx.commit('setIsLoading', true)

            let stringId = ''

            if (data.id) {
                stringId += `/${data.id}`
            }

            const res = await apiFetch({
                url: `app/${ctx.rootGetters.project.slug}/${data.model}${stringId}`,
                method: 'get',
            })

            if (res.ok) {
                let json = await res.json()

                ctx.commit('setItem', json)
                ctx.commit('setIsLoading', false)
            }
        },

        async getList (ctx, data) {
            ctx.commit('setIsLoading', true)

            const res = await apiFetch({
                url: `app/${ctx.rootGetters.project.slug}/${data.model}`,
                method: 'get',
                params: data.params ? data.params : null
            })

            if (res.ok) {
                let json = await res.json()

                ctx.commit('setList', json.results)
                ctx.commit('setRows', json.count)
                ctx.commit('setIsLoading', false)
            }
        },

        async deleteItem (ctx, data) {
            ctx.commit('setIsLoading', true)

            const res = await apiFetch({
                url: `app/${ctx.rootGetters.project.slug}/${data.model}/${data.id}`,
                method: 'delete'
            })

            if (res.ok) {
                ctx.commit('setItem', null)
                ctx.commit('setIsLoading', false)

                ctx.commit('setNotice', {
                    text: 'Запись успешно удалена',
                    type: 'success'
                })

                data.router.push({ path: '/site/' + data.model })
            }
        },

        async getEventMembers (ctx, data) {
            ctx.commit('setIsLoading', true)

            const res = await apiFetch({
                url: `app/${data.vm.project.slug}/event-members`,
                method: 'get',
                params: data.params ? data.params : null,
            })

            if (res.ok) {
                const json = await res.json()

                ctx.commit('setList', json.results)
                ctx.commit('setRows', json.count)
                ctx.commit('setIsLoading', false)
            }
        },

        async downloadEventMembers (ctx, data) {
            ctx.commit('setIsLoading', true)

            const res = await apiFetch({
                url: `app/${data.vm.project.slug}/event-members/export`,
                method: 'post',
                data: {
                    'search': data.vm.filter.search,
                    'is_e_cert': data.vm.filter.isECert,
                }
            })

            if (res.ok) {
                const json = await res.json()

                ctx.commit('setIsLoading', false)

                if (json.success && json.file_url) {
                    let a = document.createElement('a')
                    a.href = json.file_url
                    a.target = '_blank'

                    document.body.appendChild(a)
                    a.click()
                    document.body.removeChild(a)
                }
            }
        }
    }
}