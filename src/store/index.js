import Vue from 'vue'
import Vuex from 'vuex'
import VueCookies from 'vue-cookies'

import copp from './modules/copp'
import site from './modules/site'
import rpk from './modules/rpk'
import events from './modules/events'

let apiUrl = 'https://platform.grebrov.space/api/'

if (process.env.NODE_ENV === 'development') {
    apiUrl = 'http://localhost:8000/api/'
}

import apiFetch from './fetch'


Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        copp,
        site,
        rpk,
        events,
    },

    state: {
        user: null,
        project: null,

        menuItems: [],

        isLoading: false,

        notice: null,

        list: [],
        item: null,

        rows: 0,
    },

    getters: {
        user (state) {
            return state.user
        },

        project (state) {
            return state.project
        },

        menuItems (state) {
            return state.menuItems
        },

        isLoading (state) {
            return state.isLoading
        },

        notice (state) {
            return state.notice
        },

        list (state) {
            return state.list
        },

        rows (state) {
            return state.rows
        },

        item (state) {
            return state.item
        }
    },

    actions: {
        async save (ctx, data) {
            ctx.commit('setIsLoading', true)

            let fData = {}

            for (let i in data.fields) {
                let field = data.fields[i]

                if (field.value) {
                    if (field.value === 'image') {
                        if (field.value.file) {
                            if (typeof(field.value.file) === 'object') fData[field.slug] = field.value.file
                        } else if (field.value.file === null && field.value.url === null) {
                            if (field.value.file === null) fData[field.slug] = ''
                        }
                    } else if (field.type === 'checkbox') {
                        fData[field.slug] = field.value ? 1 : 0
                    } else {
                        fData[field.slug] = field.value
                    }
                }
            }

            let stringId = ''

            if (data.id) {
                stringId += `/${data.id}`
            }

            let url = ""

            if (data.module) url += data.module + '/'
            if (data.model) url += data.model

            const res = await apiFetch({
                url: `${url}${stringId}`,
                method: data.id ? 'put' : 'post',
                data: fData,
            })

            ctx.commit('setIsLoading', false)

            if (res.ok) {
                ctx.dispatch('getModelList', {
                    module: data.module,
                    model: data.model,
                })
                
                ctx.commit('setNotice', {
                    text: 'Запись успешно сохранена',
                    type: 'success',
                })

                ctx.commit('setItem', null)
            } else {
                ctx.commit('setNotice', null)
            }
        },

        async checkAuth (ctx) {
            if (VueCookies.get('token')) {
                const res = await fetch(apiUrl + 'me', {
                    method: 'get',
                    headers: {
                        Authorization: 'Token ' + VueCookies.get('token')
                    }
                })

                if (res.ok) {
                    const json = await res.json()

                    if (json.username) {
                        ctx.commit('setUser', json)

                        if (json.username === 'admin') {
                            ctx.commit('setMenuItems', [
                                { label: 'UI', path: '/ui' },
                                { label: 'Проекты', path: '/projects' },
                                { label: 'Пользователи', path: '/users' },
                                { label: 'Группы', path: '/groups' },
                            ])
                        }

                        if (json.projects && json.projects.length > 0) {
                            ctx.dispatch('getProject', json.projects[0])
                        }
                    } else {
                        ctx.commit('setUser', null)
                    }
                } else {
                    ctx.commit('setUser', null)
                }
            }
        },

        async login (ctx, data) {
            const res = await fetch(apiUrl + 'auth', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            })

            const json = await res.json()

            if (json.token) {
                VueCookies.set('token', json.token)
                
                ctx.dispatch('checkAuth')
            }
        },

        logout (ctx) {
            ctx.commit('setUser', null)
            VueCookies.remove('token')
        },

        async getProject (ctx, id) {
            const res = await fetch(apiUrl + 'projects/' + id, {
                method: 'get',
                headers: {
                    'Authorization': 'Token ' + VueCookies.get('token')
                },
            })

            if (res.ok) {
                const json = await res.json()

                if (json.slug === 'copp') {
                    ctx.commit('setMenuItems', [
                        { label: 'Соцконтракты', path: '/copp/sc' },
                        { label: 'Содейтвие занятости', path: '/copp/busyness' },
                        { label: 'Мероприятия', path: '/events' }                   
                    ])
                } else if (json.slug === 'rpk') {
                    ctx.commit('setMenuItems', [
                        { label: 'Направления', path: '/rpk/directions' },
                        { label: 'Проекты', path: '/rpk/industry-projects' },
                        { label: 'Ведомства', path: '/rpk/departments' },
                        { label: 'Работодатели', path: '/rpk/employers' },
                        { label: 'ОО', path: '/rpk/institutions' },
                        { label: 'Программы', path: '/rpk/programs' },
                        { label: 'Пользователи', path: '/rpk/persons' }
                    ])
                } else if (json.type === 'event') {
                    ctx.commit('setMenuItems', [
                        { label: 'Участники', path: '/site/event-members' },
                    ])
                }

                if (json.modules) {
                    let modules = json.modules.split('\n')

                    if (modules.includes('site')) {
                        ctx.commit('setMenuItems', [
                            { label: 'Страницы', path: '/site/pages' },
                            { label: 'Файлы', path: '/site/attachments' },
                        ])
                    }
                }

                ctx.commit('setProject', json)
            }
        },

        async getModelList (ctx, data) {
            ctx.commit('setIsLoading', true)

            let url = ""

            if (data.module) url += data.module + '/'
            if (data.model) url += data.model

            const res = await apiFetch({
                url: `${url}`,
                method: 'get',
                params: data.params,
            })

            if (res.ok) {
                let json = await res.json()

                ctx.commit('setIsLoading', false)
                ctx.commit('setList', json.results)
                ctx.commit('setRows', json.count)
            }
        },

        async getModelItem (ctx, data) {
            ctx.commit('setIsLoading', true)
            let url = ''
            if (data.module) url += data.module + '/'
            if (data.model) url += data.model + '/' + data.id

            const res = await apiFetch({
                url,
                method: 'get',
            })

            if (res.ok) {
                const json = await res.json()

                ctx.commit('setIsLoading', false)
                ctx.commit('setItem', json)
            }
        },

        async deleteItem (ctx, data) {
            ctx.commit('setIsLoading', true)

            let url = ''
            if (data.module) url += data.module + '/'
            if (data.model) url += data.model + '/' + data.id

            const res = await apiFetch({
                url,
                method: 'delete'
            })

            if (res.ok) {
                ctx.commit('setItem', null)
                ctx.commit('setIsLoading', false)

                ctx.commit('setNotice', {
                    text: 'Запись успешно удалена',
                    type: 'success'
                })

                ctx.dispatch('getModelList', {
                    module: data.module,
                    model: data.model,
                })
            }
        }
    },

    mutations: {
        setUser (state, value) {
            state.user = value
        },

        setProject (state, value) {
            state.project = value
        },

        setMenuItems (state, value) {
            state.menuItems = value
        },

        setIsLoading (state, value) {
            state.isLoading = value
        },

        setNotice (state, value) {
            state.notice = value
        },

        setList (state, value) {
            state.list = value
        },

        setRows (state, value) {
            state.rows = value
        },

        setItem (state, value) {
            state.item = value
        }
    }
})