let apiUrl = 'https://platform.grebrov.space/api/'

if (process.env.NODE_ENV === 'development') {
    apiUrl = 'http://localhost:8000/api/'
}

import VueCookies from 'vue-cookies'
import queryString from 'query-string'

export default async function (params) {
    const formData = new FormData()

    if (params.data) {
        for (let key in params.data) {
            if (
                params.data[key] !== null ||
                params.data[key] !== ''
            ) {
                formData.append(key, params.data[key])
            }
        }
    }

    let q = ''

    if (params.params) {
        q += '?'
        q += queryString.stringify(params.params)
    }

    let res = await fetch(`${apiUrl}${params.url}/${q}`, {
        method: params.method,
        headers: {
            Authorization: 'Token ' + VueCookies.get('token')
        },
        body: params.method === 'post' || params.method === 'put' ? formData : null,
    })

    return res
}