import Site from '@/pages/site'
import SiteSections from '@/pages/site/sections'
import SiteSectionsList from '@/pages/site/sections/list'
import SiteSectionsEdit from '@/pages/site/sections/edit'

import SiteEvents from '@/pages/site/events'
import SiteEventsList from '@/pages/site/events/list'

import EventMembers from '@/pages/site/event-members'

import Pages from '@/pages/site/pages'
import PagesList from '@/pages/site/pages/list'
import PagesEdit from '@/pages/site/pages/edit'

import Attachments from '@/pages/site/attachments'

export default {
    path: '/site',
    name: 'site',
    component: Site,
    children: [
        {
            path: 'sections',
            component: SiteSections,
            children: [
                {
                    path: '/',
                    name: 'site-sections',
                    component: SiteSectionsList,
                },
                {
                    path: 'create',
                    name: 'site-sections-create',
                    component: SiteSectionsEdit,
                },
                {
                    path: ':id',
                    name: 'site-sections-edit',
                    component: SiteSectionsEdit,
                }
            ]
        },
        {
            path: 'event-members',
            component: EventMembers,
            name: 'event-members',
        },
        {
            path: 'pages',
            component: Pages,
            children: [
                {
                    path: '/',
                    name: 'site-pages',
                    component: PagesList,
                },
                {
                    path: 'create',
                    name: 'site-pages-create',
                    component: PagesEdit,
                },
                {
                    path: ':id',
                    name: 'site-pages-edit',
                    component: PagesEdit,
                }
            ]
        },
        {
            path: 'attachments',
            component: Attachments,
        }
    ]
}