import Rpk from '@/pages/rpk'

import RpkDirections from '@/pages/rpk/directions'
import RpkDirectionsList from '@/pages/rpk/directions/list'
import RpkDirectionsEdit from '@/pages/rpk/directions/edit'

import RpkIndustryProjects from '@/pages/rpk/projects'
import RpkIndustryProjectsList from '@/pages/rpk/projects/list'
import RpkIndustryProjectsEdit from '@/pages/rpk/projects/edit'

import RpkDepartments from '@/pages/rpk/departments'
import RpkDepartmentsList from '@/pages/rpk/departments/list'
import RpkDepartmentsEdit from '@/pages/rpk/departments/edit'

import RpkEmployers from '@/pages/rpk/employers'
import RpkEmployersList from '@/pages/rpk/employers/list'
import RpkEmployersEdit from '@/pages/rpk/employers/edit'

import RpkInstitutions from '@/pages/rpk/institutions'
import RpkInstitutionsList from '@/pages/rpk/institutions/list'
import RpkInstitutionsEdit from '@/pages/rpk/institutions/edit'

import RpkPrograms from '@/pages/rpk/programs'
import RpkProgramsList from '@/pages/rpk/programs/list'
import RpkProgramsEdit from '@/pages/rpk/programs/edit'

import RpkPersons from '@/pages/rpk/persons'
import RpkPersonsList from '@/pages/rpk/persons/list'
import RpkPersonsDetail from '@/pages/rpk/persons/detail'

export default {
    path: '/rpk',
    name: 'rpk',
    component: Rpk,
    children: [
        {
            path: 'directions',
            component: RpkDirections,
            children: [
                {
                    path: '/',
                    name: 'rpk-directions',
                    component: RpkDirectionsList,
                },
                {
                    path: 'create',
                    name: 'rpk-directions-create',
                    component: RpkDirectionsEdit,
                },
                {
                    path: ':id',
                    name: 'rpk-directions-edit',
                    component: RpkDirectionsEdit,
                },
            ]
        },
        {
            path: 'industry-projects',
            component: RpkIndustryProjects,
            children: [
                {
                    path: '/',
                    name: 'rpk-industry-projects',
                    component: RpkIndustryProjectsList,
                },
                {
                    path: 'create',
                    name: 'rpk-industry-projects-create',
                    component: RpkIndustryProjectsEdit,
                },
                {
                    path: ':id',
                    name: 'rpk-industry-projects-edit',
                    component: RpkIndustryProjectsEdit,
                },
            ]
        },
        {
            path: 'departments',
            component: RpkDepartments,
            children: [
                {
                    path: '/',
                    name: 'rpk-departments',
                    component: RpkDepartmentsList,
                },
                {
                    path: 'create',
                    name: 'rpk-departments-create',
                    component: RpkDepartmentsEdit,
                },
                {
                    path: ':id',
                    name: 'rpk-departments-edit',
                    component: RpkDepartmentsEdit,
                },
            ]
        },
        {
            path: 'employers',
            component: RpkEmployers,
            children: [
                {
                    path: '/',
                    name: 'rpk-employers',
                    component: RpkEmployersList,
                },
                {
                    path: 'create',
                    name: 'rpk-employers-create',
                    component: RpkEmployersEdit,
                },
                {
                    path: ':id',
                    name: 'rpk-employers-edit',
                    component: RpkEmployersEdit,
                },
            ]
        },
        {
            path: 'institutions',
            component: RpkInstitutions,
            children: [
                {
                    path: '/',
                    name: 'rpk-institutions',
                    component: RpkInstitutionsList,
                },
                {
                    path: 'create',
                    name: 'rpk-institutions-create',
                    component: RpkInstitutionsEdit,
                },
                {
                    path: ':id',
                    name: 'rpk-institutions-edit',
                    component: RpkInstitutionsEdit,
                },
            ]
        },
        {
            path: 'programs',
            component: RpkPrograms,
            children: [
                {
                    path: '/',
                    name: 'rpk-programs',
                    component: RpkProgramsList,
                },
                {
                    path: 'create',
                    name: 'rpk-programs-create',
                    component: RpkProgramsEdit,
                },
                {
                    path: ':id',
                    name: 'rpk-programs-edit',
                    component: RpkProgramsEdit,
                },
            ]
        },

        {
            path: 'persons',
            component: RpkPersons,
            children: [
                {
                    path: '/',
                    name: 'rpk-persons',
                    component: RpkPersonsList,
                },
                {
                    path: ':id',
                    name: 'rpk-persons-detail',
                    component: RpkPersonsDetail,
                }
            ]
        }
    ]
}