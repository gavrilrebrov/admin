import Events from '@/pages/events'
import EventsList from '@/pages/events/list'
import EventsEdit from '@/pages/events/edit'
import EventsMembers from '@/pages/events/members'

import Directions from '@/pages/events/copp/directions'
import DirectionsEdit from '@/pages/events/copp/directions/edit'

import Institutions from '@/pages/events/copp/institutions'
import InstitutionsEdit from '@/pages/events/copp/institutions/edit'

import Slides from '@/pages/events/copp/slides'

export default {
    path: '/events',
    component: Events,
    children: [
        {
            path: '/',
            name: 'events',
            component: EventsList,
        },
        {
            path: 'create',
            name: 'events-create',
            component: EventsEdit,
        },
        {
            path: ':id',
            name: 'events-edit',
            component: EventsEdit,
        },
        {
            path: ':id/members',
            name: 'events-members',
            component: EventsMembers,
        },
        {
            path: ':id/copp/directions',
            name: 'events-copp-directions',
            component: Directions,
        },
        {
            path: ':id/copp/directions/create',
            name: 'events-copp-directions-create',
            component: DirectionsEdit,
        },
        {
            path: ':id/copp/directions/:directionId',
            name: 'events-copp-directions-create',
            component: DirectionsEdit,
        },

        {
            path: ':id/copp/institutions',
            name: 'events-copp-institutions',
            component: Institutions,
        },
        {
            path: ':id/copp/institutions/create',
            name: 'events-copp-institutions-create',
            component: InstitutionsEdit,
        },
        {
            path: ':id/copp/institutions/:institutionId',
            name: 'events-copp-institutions-create',
            component: InstitutionsEdit,
        },

        {
            path: ':id/copp/slides',
            name: 'events-copp-slides',
            component: Slides,
        }
    ]
}