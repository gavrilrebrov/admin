import Copp from '@/pages/copp'
import CoppSc from '@/pages/copp/sc'
import CoppScInstitutions from '@/pages/copp/sc/institutions'
import CoppScInstitutionsEdit from '@/pages/copp/sc/institutions/edit'
import CoppScInstitutionsList from '@/pages/copp/sc/institutions/list'

import CoppScRequests from '@/pages/copp/sc/requests'

import CoppScPrograms from '@/pages/copp/sc/programs'
import CoppScProgramsEdit from '@/pages/copp/sc/programs/edit'
import CoppScProgramsList from '@/pages/copp/sc/programs/list'
import CoppScProgramsImport from '@/pages/copp/sc/programs/import'

import CoppBusyness from '@/pages/copp/busyness'
import CoppBusynessRequests from '@/pages/copp/busyness/requests/list'
import CoppBusynessRequest from '@/pages/copp/busyness/requests/detail'
import CoppBusynessTutors from '@/pages/copp/busyness/tutors/list'
import CoppBusynessTutorsEdit from '@/pages/copp/busyness/tutors/edit'


export default {
    path: '/copp',
    name: 'copp',
    component: Copp,
    children: [
        {
            path: 'sc',
            name: 'copp-sc',
            redirect: { path: '/copp/sc/requests' },
            component: CoppSc,
            children: [
                {
                    path: 'requests',
                    name: 'copp-sc-requests',
                    component: CoppScRequests,
                },
                {
                    path: 'institutions',
                    component: CoppScInstitutions,
                    children: [
                        {
                            path: '/',
                            name: 'copp-sc-institutions',
                            component: CoppScInstitutionsList
                        },
                        {
                            path: 'create',
                            name: 'copp-sc-institutions-create',
                            component: CoppScInstitutionsEdit,
                        },
                        {
                            path: ':id',
                            name: 'copp-sc-institutions-edit',
                            component: CoppScInstitutionsEdit,
                        }
                    ]
                },
                {
                    path: 'programs',
                    component: CoppScPrograms,
                    children: [
                        {
                            path: 'create',
                            name: 'copp-sc-programs-create',
                            component: CoppScProgramsEdit,
                        },
                        {
                            path: '/',
                            name: 'copp-sc-programs',
                            component: CoppScProgramsList,
                        },
                        {
                            path: 'import',
                            name: 'copp-sc-programs-import',
                            component: CoppScProgramsImport,
                        },
                        {
                            path: ':id',
                            name: 'copp-sc-programs-edit',
                            component: CoppScProgramsEdit,
                        },
                    ]
                }
            ]
        },
        {
            path: 'busyness',
            name: 'copp-busyness',
            redirect: { path: '/copp/busyness/requests' },
            component: CoppBusyness,
            children: [
                {
                    path: 'requests',
                    name: 'copp-busyness-requests',
                    component: CoppBusynessRequests,
                },
                {
                    path: 'requests/:id',
                    name: 'copp-busyness-request',
                    component: CoppBusynessRequest,
                },
                {
                    path: 'tutors',
                    name: 'copp-busyness-tutors',
                    component: CoppBusynessTutors,
                },
                {
                    path: 'tutors/create',
                    name: 'copp-busyness-tutors-create',
                    component: CoppBusynessTutorsEdit,
                },
                {
                    path: 'tutors/:id',
                    name: 'copp-busyness-tutors-edit',
                    component: CoppBusynessTutorsEdit,
                },

            ]
        }
    ]
}