import Vue from 'vue'
import VueRouter from 'vue-router'

import copp from './modules/copp'
import site from './modules/site'
import ui from './modules/ui'
import rpk from './modules/rpk'
import events from './modules/events'

import Projects from '@/pages/admin/projects'

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'hash',
    routes: [
        copp,
        site,
        ui,
        rpk,
        events,

        {
            path: '/projects',
            name: 'projects',
            component: Projects,
        }
    ]
})