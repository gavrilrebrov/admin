import Vue from 'vue'
import App from './app'
import store from './store'
import router from './router'

import '../node_modules/normalize.css/normalize.css'
import 'vue-select/dist/vue-select.css'
import './assets/fonts/Montserrat.css'
import './assets/scss/main.scss'

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')